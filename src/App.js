import React from "react";
import Chat from "./chat/index.js";
import "./App.css";
import "./assets/nes.min.css";

function App() {
  return (
    <div className="App">
      <div className="main">
        <Chat />
      </div>
    </div>
  );
}

export default App;
