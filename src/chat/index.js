import React, { useState, useEffect, useRef } from "react";
import socketIOClient from "socket.io-client";
import axios from "axios";

import "./index.css";
const ENDPOINT = "//cripto-live.herokuapp.com";
// const ENDPOINT = "//localhost:8080";
const ADMIN = "CAPIVARA ATOMICA DOIS MIL E VINTE";

const socket = socketIOClient(ENDPOINT);

export default (props) => {
  const [input, setInput] = useState("");
  const [userInput, setUserInput] = useState("");
  const [user, setUser] = useState(null);
  const [deleteDialog, setDeleteDialog] = useState(null);
  const [messages, setMessages] = useState([]);
  const messagesEndRef = useRef(null);

  // useEffect(() => {
  //   if (user && messages.length === 0) {
  //     axios
  //       .get(ENDPOINT + "/history")
  //       .then((response) => {
  //         setMessages(response.data);
  //         scrollToBottom({ behavior: "smooth" });
  //       })
  //       .catch((err) => console.log("err", err));
  //   }
  // }, [user]);

  useEffect(() => {
    socket.on("NEW MESSAGE", (msg) => {
      setMessages([...messages, msg]);
      scrollToBottom();
    });

    socket.on("NEW HISTORY", (msgs) => {
      setMessages(msgs);
      scrollToBottom();
    });

    socket.on("REGISTER", (username) => {
      if (username) {
        setUser(username);
      } else {
        alert("nome já em uso! Escolha outro por favor");
      }
    });
  }, [messages]);

  const scrollToBottom = (options) => {
    if (messagesEndRef.current) {
      messagesEndRef.current.scrollIntoView({
        behavior: "auto",
        block: "end",
        ...options,
      });
    }
  };

  const sendDeleteMessage = (event) => {
    event.preventDefault();
    socket.emit("DELETE MESSAGE", deleteDialog);
    setDeleteDialog(null);
  };

  const sendBanUser = (event) => {
    event.preventDefault();
    socket.emit("BAN USER", deleteDialog.user);
    setDeleteDialog(null);
  };

  const sendMessage = (event) => {
    event.preventDefault();
    socket.emit("SENT MESSAGE", { user, content: input });
    setInput("");
  };

  const createUser = (event) => {
    event.preventDefault();
    socket.emit("REGISTER", { username: userInput });
    setUserInput("");
  };

  return (
    <div class="chat-wrapper nes-container with-title is-dark">
      <p class="title">CHAT DA CRIPTOGOMA</p>
      <div class="chat">
        {user ? (
          <>
            <ul class=" message-list nes-list is-circle is-dark">
              {messages.map((message, index) => {
                return (
                  <li
                    onClick={() => {
                      if (user === ADMIN) {
                        setDeleteDialog(message);
                      }
                    }}
                  >
                    <span class="nes-text is-primary is-bold">
                      {message.user}:
                    </span>{" "}
                    {message.content}
                  </li>
                );
              })}
              <div ref={messagesEndRef}></div>
            </ul>

            <form onSubmit={sendMessage}>
              <textarea
                class="nes-textarea"
                placeholder="Digite aqui sua mensagem"
                onKeyDown={(event) => {
                  if (event.keyCode == 13 && event.shiftKey == false) {
                    sendMessage(event);
                  }
                }}
                type="text"
                value={input}
                onChange={(ev) => setInput(ev.target.value)}
              />
              <button class="nes-btn is-primary" type="submit">
                Enviar
              </button>
            </form>

            {deleteDialog && (
              <dialog class="moderation-dialog">
                <form class="nes-dialog is-dark" id="dialog-dark">
                  <p class="title">Quer censurar essa mensagem?</p>
                  <menu class="dialog-menu">
                    <button
                      class="nes-btn"
                      onClick={() => setDeleteDialog(null)}
                    >
                      Deixa quieto
                    </button>
                    <button
                      class="nes-btn is-primary"
                      onClick={sendDeleteMessage}
                    >
                      Quero
                    </button>
                    <button class="nes-btn is-primary" onClick={sendBanUser}>
                      Quero mutar essa pessoa
                    </button>
                  </menu>
                </form>
              </dialog>
            )}
          </>
        ) : (
          <div class="nes-container with-title is-dark">
            <p class="title">Escolha um nome para usar</p>
            <form onSubmit={createUser}>
              <input
                class="nes-input is-dark"
                placeholder="Capivara da Silva"
                type="text"
                value={userInput}
                onChange={(ev) => setUserInput(ev.target.value)}
              />
              <button class="nes-btn is-primary" type="submit">
                Entrar
              </button>
            </form>
          </div>
        )}
      </div>
    </div>
  );
};
